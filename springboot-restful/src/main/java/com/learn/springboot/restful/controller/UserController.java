package com.learn.springboot.restful.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.learn.springboot.restful.exceptions.UserNotFoundException;
import com.learn.springboot.restful.model.User;
import com.learn.springboot.restful.service.UserDaoService;

@RestController
public class UserController {
	
	@Autowired
	private UserDaoService service;

	@GetMapping(path = { "/users" })
	public List<User> retriveAllUsers() {
		System.out.println(service.findAll());
		return service.findAll();
	}

	@GetMapping("/users/{id}")
	public EntityModel<User> retriveUser(@PathVariable Integer id) throws UserNotFoundException {
		User user = service.findOne(id);
		if (user == null) {
			throw new UserNotFoundException("Id: " + id);
		}
		
		//"all-users", SERVER_PATH + "/users"
		//retrieveAllUsers
		//HATEOAS
		EntityModel<User> resource = EntityModel.of(user); // new EntityModel<User>(user.get());
		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retriveAllUsers());
		resource.add(linkTo.withRel("all-users"));

		return resource; 
	}

	@PostMapping("/users")
	public ResponseEntity<Object> save(@Valid @RequestBody User user) {
		User savedUser = service.save(user);

		// "/users/{id}" --> id = savedUser.getId();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId())
				.toUri();

		return ResponseEntity.created(location).build();

	}

	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable Integer id) throws UserNotFoundException {
		User user = service.deleteOne(id);
		if (user == null) {
			throw new UserNotFoundException("Id: " + id);
		}
	}

}
