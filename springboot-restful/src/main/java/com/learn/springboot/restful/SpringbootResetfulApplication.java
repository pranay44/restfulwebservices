package com.learn.springboot.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootResetfulApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringbootResetfulApplication.class, args);
	}

}
