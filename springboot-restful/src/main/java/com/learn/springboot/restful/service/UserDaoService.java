package com.learn.springboot.restful.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.learn.springboot.restful.model.User;


@Component
public class UserDaoService {
	
private static List<User>  users = new ArrayList<>();
	
	private static int usersCount = 103;
	
	static {
		users.add(new User(101, "Buckley", new Date()));
		users.add(new User(102, "Johnson", new Date()));
		users.add(new User(103, "Adam", new Date()));
	}
	
	public List<User> findAll() {
		return users;
	}
	
	public User findOne(int id) {
		for(User user: users) {
			if(user.getId()==id) {
				return user;
			}
		}
		return null;
	}
	
	public User save(User user) {
		if(user.getId() == null) {
			user.setId(++usersCount);
		}
		
		users.add(user);
		return user;
	}
	
	public User deleteOne(int id) {
		Iterator<User> iterator = users.iterator();
		while(iterator.hasNext()) {
			User user = iterator.next();
			if(user.getId() == id) {
				iterator.remove();
				return user;
			}
		}
		return null;
	}

}
