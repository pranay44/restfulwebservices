package com.learn.springboot.restful.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.learn.springboot.restful.model.HelloWorldBean;


@RestController
public class HelloWorldController {
	
//	@RequestMapping(path = "/hello-world", method = RequestMethod.GET)
	@GetMapping(path = "/hello-world")
	public String helloWorld() {
		return "Hello World";
	}
	
	@GetMapping(path = "/hello-world-bean")
	public HelloWorldBean helloWorldBean() {
		return new HelloWorldBean("hello World");
	}
	
	//	"/hello-world/welcome to springboot"
	@GetMapping(path = "/hello-world/{name}")
	public HelloWorldBean helloWorldPathVariable(@PathVariable String name) {
		return new HelloWorldBean(String.format("hello World, %s", name));
	}

}
